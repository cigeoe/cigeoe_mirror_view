# CIGeoE Mirror View



# Installation

For QGIS 3:

There are two methods to install the plugin:

- Method 1:

  1- Copy the folder “cigeoe_reshape_features_3d” to folder:

  ```console
  [drive]:\Users\[user]\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins
  ```

  2 - Start QGis and go to menu "Plugins" and select "Manage and Install Plugins"

  ![ALT](./images/image01.png)

  3 - Select “CIGeoE Mirror View”

  ![ALT](./images/image02.png)

  4 - After confirm the operation the plugin will be available in toolbar


- Method 2:

  1 - Compress the folder “cigeoe_mirror_view” (.zip).

  2 - Start QGis and go to "Plugins" and select “Manage and Install Plugins”

  3 - Select “Install from Zip” (choose .zip generated in point 1) and then select "Install Plugin"

# Usage

1 - Click the plugin icon. The mirror view panel will show up on the screen. If a layer is prior selected on the "Layers panel", the plus button icon in the mirror view panel appears green. 

![ALT](./images/image03.png)

2 - Clicking the green plus button adds a "synchronized layer" with the current active layer. The plus button turns off after this step. At the same time, the "x" button initially off turns red and active, allowing to remove the "synchronized layer".

![ALT](./images/image04.png)

3 - The mirror view panel footer has buttons for functionalities like "render" and "scale factor" and a tool to rename the panel.

![ALT](./images/image05.png)

4 - After closing the mirror view panel, you can reopen it by clicking the toolbar using the right mouse button and selecting "mirror views" in the "Panels" section.

![ALT](./images/image06.png)


# Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

# License

[AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)
