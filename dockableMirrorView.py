# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CIGeoEMirrorView
                                 A QGIS plugin
 Add one or more panels synchronized with the main panel.
Adapted from plugin DockableMirrorMap (homepage=http://www.faunalia.com/)

 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2019-12-04
        git sha              : $Format:%H$
        copyright            : (C) 2019 by Centro de Informação Geoespacial do Exército 
        email                : igeoe@igeoe.pt
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from qgis.gui import *
from qgis.core import *

from .mirrorView import MirrorView

class DockableMirrorView(QDockWidget):

	TITLE = "MirrorView"

	closed = pyqtSignal('PyQt_PyObject')        
	

	def __init__(self, parent, iface):
		QDockWidget.__init__(self, parent)

		self.mainWidget = MirrorView(self, iface)
		self.location = Qt.RightDockWidgetArea
		self.number = -1

		self.setupUi()
		
		#self.connect(self, SIGNAL("dockLocationChanged(Qt::DockWidgetArea)"), self.setLocation)  
		self.dockLocationChanged.connect(self.setLocation)					
		self.iface = iface
		
		
	def closeEvent(self, event):
		#self.emit( SIGNAL( "closed(PyQt_PyObject)" ), self )						
		self.closed.emit(self)													
		return QDockWidget.closeEvent(self, event)


	def setNumber(self, n=-1):
		self.number = n		
		self.updateLabel()


	def getMirror(self):
		return self.mainWidget


	def getLocation(self):
		return self.location


	def setLocation(self, location):
		self.location = location


	def setupUi(self):
		self.setObjectName( "dockablemirrorview_dockwidget" )
		self.updateLabel()
		self.setWidget(self.mainWidget)


	def updateLabel(self):
		title = "%s : %s" % (self.TITLE, self.number) if self.number >= 0 else self.TITLE
		if len(self.mainWidget.label) != 0:
			title += "  " + self.mainWidget.label
		self.setWindowTitle( title )
				
